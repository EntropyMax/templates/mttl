
# Training Roadmap Layout
* The training [roadmap](https://90cos.gitlab.io/mttl/#/roadmap) takes the form of a graph where each node represents a work role in the 90th COS.  
* The work roles on the farthest left side are work roles that do not have another work role as a prerequisite.
* The lines connecting work roles represent the available paths to transition from one work role to another.
* Any work role that does not have lines connecting to the right side does not lead to a more advanced work role.
* Specializations are displayed as work roles in the training roadmap diagram.

# Required Items
* Clicking the name of a work role will display a list of items needed to complete that work role. These items will be required courses and/or required evaluation.
* Items in the work role list can be clicked to navigate to a page to sign up for any course or evaluation.
* If there is additional supplementary content for a required item, the arrow next to the item will show additional content for that role. Clicking these links will display content for review.

# OJT Printout
A list of On the Job training items can be obtained by clicking the OJT option on a given work role. Additional information on the OJT checklist can be found [here](how-to-find-and-accomplish-ojt.md)