# Work Role Upgrade
1.  Work Role/Specialization upgrade is identified
2.  Primary and additional trainers are assigned
    *  Assigned IAW 90 COS flight commanders
    *  Documented on individual JQS
    *  Trainer/Supervisor/Trainee Prints JQS and fills out Trainer fields
3.  Courses are completed
    *  Request through JIRA Service Desk
    *  Tasks covered by the course are considered training complete and documented with a completion certificate
    *  JQS signed off by 90 COS Training Manager / uploaded to JIRA ticket / documented on AF 4419/4420
4.  OJT tasks are completed
    *  Tasks are reviewed/signed off by Trainee/Trainer
    *  Review/transcribe old JQS forms to new JQS forms annually
    *  Prior to submitting for completion, a new JQS form is downloaded/transcribed and new tasks are completed
5.  Experience completed IAW Vol 1
    *  Experience documented by Trainer/Supervisor/Meeting organizer on AF Form 4419/4420 or JQS TBD
6.  Supervisor submits documentation to 90 COS/CYT Training Element
    *  Course Completion Certs/Documentation
    *  Completed JQS
    *  Experience Documentation
7.  Required evals completed IAW Vol 2
    *  Request through JIRA Service Desk
    *  Evals documented on AF Form 4418 and AF Form 4420
    *  If required by Examiner, remedial actions (RA) and additional training completed and documented on AF Form 4418 
8.  Course completion documents and AF Forms 4418, 4419, and 4420 are archived in the the Electronics Records Management (ERM) Drive
9.  Letter of X (LOX) Qualification List is updated for those with a qualification rating of Q1 or Q2
