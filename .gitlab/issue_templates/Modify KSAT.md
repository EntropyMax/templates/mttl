### Which KSAT(s) needs to be changed/deleted?:
KSAT 1 ID: 
KSAT 2 ID:

### Changes
- Present Value:
- New Value:
e.g. change the description to "must know how to use the Python crypto library"

### Reason
Why does the KSAT(s) need to be changed?
- Grammar/Punctuation
- No longer required for the job
- Needs to be more specific
- Other (Please explain below)

/label ~"mttl::ksat" ~customer ~"office::CYT" ~"backlog::idea"
/milestone %"CYT Backlog"
