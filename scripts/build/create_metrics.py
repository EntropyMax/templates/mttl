#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
import mongo_helpers

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
wrs = db.work_roles

output_root = 'frontend/src/data'

find_aggregate = [
    {
        '$lookup': {
            'from': 'work_roles',
            'localField': 'ksat_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }
    }
]
find_aggregate_both = [
    {
        '$lookup': {
            'from': 'rel_links',
            'localField': '_id',
            'foreignField': 'KSATs.ksat_id',
            'as': 'rel-links'
        }
    }
]

def update_metrics(metrics:dict, key:str, with_both:int, with_training:int, with_eval:int, total:int, without_training_set:list, without_eval_set:list):
    metrics.update({ key: { 
        'percent-total': (with_both / total) * 100 if total > 0 else 0,
        'percent-training': (with_training / total) * 100 if total > 0 else 0,
        'percent-eval': (with_eval / total) * 100 if total > 0 else 0,
        'ksas-without-training': without_training_set,
        'ksas-without-eval': without_eval_set,
        'count-total': total,
        'count-training-eval': with_both,
        'count-training': with_training,
        'count-eval': with_eval
    }})

def create_mttl_metrics(metrics:dict):
    '''
    create mttl metrics in the 'metrics' dict
    '''

    total_set = set()
    with_both_set = set()
    without_training_set = set()
    without_eval_set = set()

    for ksat in reqs.aggregate(find_aggregate_both):
        ksat_id = ksat['ksat_id']
        total_set.add(ksat_id)
        without_training_set.add(ksat_id)
        without_eval_set.add(ksat_id)
        if len(ksat['rel-links']) == 0:
            continue
        for rl in ksat['rel-links']:
            if rl['map_for'] == 'training' and ksat_id in without_training_set:
                without_training_set.remove(ksat_id)
            if rl['map_for'] == 'eval' and ksat_id in without_eval_set:
                without_eval_set.remove(ksat_id)
        if ksat_id not in without_training_set and ksat_id not in without_eval_set:
            with_both_set.add(ksat_id)

    without_training_list = list(without_training_set)
    without_eval_list = list(without_eval_set)
    without_training_list.sort()
    without_eval_list.sort()

    update_metrics(metrics, 
        'MTTL', 
        len(with_both_set), 
        len(total_set.difference(without_training_set)), 
        len(total_set.difference(without_eval_set)), 
        len(total_set), 
        without_training_list, 
        without_eval_list)

def append_ttl_metrics(metrics:dict, wrspec:str):
    '''
    create ttl metrics in the 'metrics' dict
    '''

    wr_match = [
        {
            '$match': {'work-roles.work-role': wrspec}
        }
    ]

    total_set = set()
    with_both_set = set()
    without_training_set = set()
    without_eval_set = set()

    for ksat in reqs.aggregate(find_aggregate + wr_match + find_aggregate_both):
        ksat_id = ksat['ksat_id']
        total_set.add(ksat_id)
        without_training_set.add(ksat_id)
        without_eval_set.add(ksat_id)
        if len(ksat['rel-links']) == 0:
            continue
        for rl in ksat['rel-links']:
            if rl['map_for'] == 'training' and ksat_id in without_training_set:
                without_training_set.remove(ksat_id)
            if rl['map_for'] == 'eval' and ksat_id in without_eval_set:
                without_eval_set.remove(ksat_id)
        if ksat_id not in without_training_set and ksat_id not in without_eval_set:
            with_both_set.add(ksat_id)

    if 'work-roles' not in metrics:
        metrics['work-roles'] = {}

    without_training_list = list(without_training_set)
    without_eval_list = list(without_eval_set)
    without_training_list.sort()
    without_eval_list.sort()

    update_metrics(metrics['work-roles'], 
        wrspec, 
        len(with_both_set), 
        len(total_set.difference(without_training_set)), 
        len(total_set.difference(without_eval_set)), 
        len(total_set), 
        without_training_list, 
        without_eval_list)

def create_ttl_metrics(metrics:dict, work_roles:list):
    '''
    Starter function for ttl metrics to pass individual work-roles/specializations into append_ttl_metrics
    '''
    for work_role in work_roles:
        append_ttl_metrics(metrics, work_role)
    

def create_nowrspec_metrics(metrics:dict):
    without_wrspec = len(list(reqs.aggregate([\
        {'$lookup': {
            'from': 'work_roles',
            'localField': '_id',
            'foreignField': 'ksat_id',
            'as': 'work-roles'
        }},
        {'$match': {'work-roles': {'$size': 0} }}
    ])))
    total = reqs.count_documents({})

    metrics.update({ 'nowrspec': {
        'percent-total': (without_wrspec / total) * 100 if total > 0 else 0
    }})

def create_jct_cs_metrics(metrics:dict):
    '''
    create jct&cs metrics in the 'metrics' dict
    '''

    total_set = set()
    with_both_set = set()
    without_training_set = set()
    without_eval_set = set()

    for ksat in reqs.aggregate(find_aggregate_both):
        if('JCT&CS' in ksat['requirement_src']):
            ksat_id = ksat['ksat_id']
            total_set.add(ksat_id)
            without_training_set.add(ksat_id)
            without_eval_set.add(ksat_id)
            if len(ksat['rel-links']) == 0:
                continue
            for rl in ksat['rel-links']:
                if rl['map_for'] == 'training' and ksat_id in without_training_set:
                    without_training_set.remove(ksat_id)
                if rl['map_for'] == 'eval' and ksat_id in without_eval_set:
                    without_eval_set.remove(ksat_id)
            if ksat_id not in without_training_set and ksat_id not in without_eval_set:
                with_both_set.add(ksat_id)
        
    without_training_list = list(without_training_set)
    without_eval_list = list(without_eval_set)
    without_training_list.sort()
    without_eval_list.sort()

    update_metrics(metrics, 
        'JCT&CS', 
        len(with_both_set), 
        len(total_set.difference(without_training_set)), 
        len(total_set.difference(without_eval_set)), 
        len(total_set), 
        without_training_list, 
        without_eval_list)

def main():
    global output_root
    
    metrics = {}
    output_path = os.path.join(output_root, 'metrics.min.json')

    work_roles = list(wrs.distinct('work-role'))

    create_mttl_metrics(metrics)
    create_ttl_metrics(metrics, work_roles)
    create_nowrspec_metrics(metrics)
    create_jct_cs_metrics(metrics)

    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w') as metrics_file:
        json.dump(metrics, metrics_file, sort_keys=False, separators=(',', ':'))

if __name__ == "__main__":
    main()