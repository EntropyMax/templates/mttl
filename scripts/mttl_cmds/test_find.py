#!/usr/bin/python3

import os
import argparse
import unittest
import subprocess
import pymongo
import json
import random
from find import find_query

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links


class TestMethods(unittest.TestCase):

    def test_find(self):
        with open('requirements/KNOWLEDGE.json', 'r') as k_file:
            k_list = json.load(k_file)
            for i in range(10):
                test_item = k_list[random.randrange(0, len(k_list)) - 1]
                found_item = find_query(reqs, 'ksat', {"_id": test_item['_id']}, None, False)
                self.assertEqual(test_item['_id'], found_item[0]['_id'])
                self.assertEqual(test_item['description'], found_item[0]['description'])
                self.assertEqual(test_item['topic'], found_item[0]['topic'])

    def test_find_projection(self):
        with open('requirements/KNOWLEDGE.json', 'r') as k_file:
            k_list = json.load(k_file)
            for i in range(10):
                test_item = k_list[random.randrange(0, len(k_list)) - 1]
                found_item = find_query(reqs, 'ksat', {"_id": test_item['_id']}, ['_id'], False)
                self.assertEqual(test_item['_id'], found_item[0]['_id'])
                self.assertEqual(len(found_item[0].keys()), 1)
            for i in range(10):
                test_item = k_list[random.randrange(0, len(k_list)) - 1]
                found_item = find_query(reqs, 'ksat', {"_id": test_item['_id']}, ['_id', 'description'], False)
                self.assertEqual(test_item['_id'], found_item[0]['_id'])
                self.assertEqual(test_item['description'], found_item[0]['description'])
                self.assertEqual(len(found_item[0].keys()), 2)


if __name__ == "__main__":
    unittest.main()