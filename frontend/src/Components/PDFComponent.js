import React, { Component } from 'react';
import PDFs from '../Components/PDFIndex.js';
import './node.css';

const element = document.createElement('div');
document.body.appendChild(element);

class DownloadFile extends Component {
	
	constructor(props) {
    super(props);
    this.workrole = props.workrole;
  }
	
	render() {
		return (
       <a href={PDFs[this.workrole]} className="ojtLink" target='_blank' rel="noopener noreferrer" download>{"OJT"}</a>
		)
	}

}

export default DownloadFile;